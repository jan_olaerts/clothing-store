package be.vdab.ClothingStore;

public enum GenderTarget {
    MALES, FEMALES, NEUTRAL
}