package be.vdab.ClothingStore;

public class StringTool {

    public static String leftString(String string, int length) {
        if(string == null) return null;
        if(length <= 0) return "";
        if(length > string.length()) return string;
        return string.substring(0, length);
    }

    public static String cleanBarcode(String barcode) {
        if(barcode == null) return null;
        barcode = barcode.replace(" ", "").replace("-", "");
        StringBuilder sb = new StringBuilder(barcode);
        try {
            sb.insert(1, " ");
            sb.insert(8, " ");
        } catch(Exception e) {
            System.err.println("Give at least 8 numbers");
            return "";
        }
        return sb.toString();
    }

    public static boolean isBarcode(String barcode) {
        if(barcode == null) return false;
        if(barcode.length() != 15) return false;
        if(!barcode.matches("\\d [\\d]{6} [\\d]{6}")) return false;
        return true;
    }
}