package be.vdab.ClothingStore;

import java.util.ArrayList;
import java.util.Collection;

public class ClothingStore {

    private Collection<Product> products;
    private String productOverview;

    public ClothingStore() {
        this.products = new ArrayList<>();
        this.productOverview = "";
    }

    public void registerProduct(Product product) {
        products.add(product);
    }

    public boolean removeProduct(Product product) {
        return products.remove(product);
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public Product getProductByBarcode(String barcode) {
        Product product = null;
        for(Product item : products) {
            if(item.getBarcode().equals(barcode)) {
                product = item;
            }
        }

        return product;
    }
}