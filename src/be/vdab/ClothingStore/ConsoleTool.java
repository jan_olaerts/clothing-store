package be.vdab.ClothingStore;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Collection;
import java.util.Scanner;

public class ConsoleTool {

    private static Scanner scanner = new Scanner(System.in);

    public static void askPressEnterToContinue() {
        System.out.println("Press enter to continue.");
        scanner.nextLine();
    }

    public static String askUserInputString(String question) {
        System.out.println(question);
        String input = "";
        input = scanner.nextLine();
        return input;
    }

    public static String askUserInputString(String question, int minChars) {
        if(minChars <= 0) {
            System.out.println(question);
            return scanner.nextLine();
        } else {
            String input = null;
            do {
                System.out.println(question);
                input = scanner.nextLine();
                if(input.length() < minChars) System.err.println("Input must be at least " + minChars + " characters");
            } while (input.length() < minChars);

            return input;
        }
    }

    public static int askUserInputInteger(String question) {
        int input = -1;
        try {
            System.out.println(question);
            input = scanner.nextInt();
        } catch (Exception e) {
            System.err.println("Please type in a number: \n");
        } finally {
            scanner.nextLine();
        }

        return input;
    }

    public static int askUserInputInteger(String question, int min) {
        int input = 0;
        while (input < min || input == 0) {
            try {
                input = ConsoleTool.askUserInputInteger(question);
                if (input < min) System.err.println("Input must be higher than " + min);
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("Input must be a number higher than " + min);
            }
        }
        return input;
    }

    public static int askUserInputInteger(String question, int min, int max) {
        int input = 0;
        while (input < min || input > max) {
            try {
                input = askUserInputInteger(question);
                if(input < min) System.err.println("Input must be a number between " + min + " and " + max);
                else if(input > max) System.err.println("Input must be a number between " + min + " and " + max);
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("Input must be a number between " + min + " and " + max);
            }
        }

        return input;
    }

    public static float askUserInputFloat(String question) {
        float input = -1f;

        try {
            System.out.println(question);
            input = scanner.nextFloat();
        } catch (Exception e) {
            System.out.println("Please type in a number: \n");
        } finally {
            scanner.nextLine();
        }

        return input;
    }

    public static LocalDate askUserInputDate(String question) {
        LocalDate localDate = null;
        do {
            System.out.println(question);
            String date = scanner.nextLine();
            try {
                localDate = LocalDate.parse(date);
            } catch(DateTimeParseException dtpe) {
                System.err.println("Error while parsing the date " + dtpe.getMessage());
                System.err.println("Expected format: YYYY-MM-DD");
            }
        } while (localDate == null);

        return localDate;
    }

    public static LocalDate askUserInputDateBefore(String question, LocalDate maxDate) {
        LocalDate ld = askUserInputDate(question);
        while(ld.isAfter(maxDate) || ld.isEqual(maxDate)) {
            System.err.println("Date must be before " + maxDate);
            ld = askUserInputDate(question);
        }
        return ld;
    }

    public static LocalDate askUserInputDateBetween(String question, LocalDate minDate, LocalDate maxDate) {
        LocalDate ld = askUserInputDate(question);
        while (ld.isBefore(minDate) || ld.isAfter(maxDate) || ld.isEqual(minDate) || ld.isEqual(maxDate)) {
            System.err.println("Date must be between " + minDate + " and " + maxDate);
            ld = askUserInputDate(question);
        }

        return ld;
    }

    public static ClothingTypes askUserInputClothingType(String question) {
        ClothingTypes ct = null;
        int input;
        do {
            input = ConsoleTool.askUserInputInteger(question + "(1. Trousers, 2. Shirt, 3. Polo, 4. Sweater, 5. Dress)");
            if(input < 1 || input > 5) System.err.println("Type in a number between 1 and 5");
        } while (input < 1 || input > 5);

        if(input == 1) ct = ClothingTypes.TROUSERS;
        if(input == 2) ct = ClothingTypes.SHIRT;
        if(input == 3) ct = ClothingTypes.POLO;
        if(input == 4) ct = ClothingTypes.SWEATER;
        if(input == 5) ct = ClothingTypes.DRESS;

        return ct;
    }

    public static GenderTarget askUserInputGenderTarget(String question) {
        GenderTarget gt = null;
        int input;
        do {
            input = ConsoleTool.askUserInputInteger(question + "(1. MALES, 2. FEMALES, 3. NEUTRAL)");
            if(input < 1 || input > 3) System.err.println("Type in a number between 1 and 3");
        } while (input < 1 || input > 3);

        if(input == 1) gt = GenderTarget.MALES;
        if(input == 2) gt = GenderTarget.FEMALES;
        if(input == 3) gt = GenderTarget.NEUTRAL;
        return gt;
    }

    public static void printProducts(Collection<Product> products, String term) {
        System.out.println(term + ":");
        products.forEach(System.out::println);
    }
}