package be.vdab.ClothingStore;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class ConsoleApp {

    private Scanner scanner = new Scanner(System.in);
    private static ClothingStore store = new ClothingStore();

    public static void main(String[] args) {
        ConsoleApp consoleApp = new ConsoleApp();
        consoleApp.addSamples();
        consoleApp.mainMenu();
    }

    public void addSamples() {
        store.registerProduct(new Product(ClothingTypes.SHIRT, "1 123456 789123", Map.of("Polyester", 100),
                Map.of("Small", 2, "Medium", 1, "Large", 4), "SUPPL LTD.", "BRANDY", "DESIGN CO.",
                7, 12.5f, LocalDate.of(2020, 4, 15), GenderTarget.MALES));

        store.registerProduct(new Product(ClothingTypes.DRESS, "2 134567 891234", Map.of("Linen", 100),
                Map.of("Small", 5, "Medium", 0, "Large", 3), "SUPPL LTD.", "BRANDY",
                "DRESS CO.", 8, 25.65f, LocalDate.of(2020, 3, 18), GenderTarget.FEMALES));

        store.registerProduct(new Product(ClothingTypes.POLO, "3 124567 891234", "POLO CO.", 10, 15.99f,
                LocalDate.of(2020, 2, 14), GenderTarget.MALES));

        store.registerProduct(new Product(ClothingTypes.TROUSERS, "4 256897 235214", Map.of("Leather", 1, "Polyester", 100),
                Map.of("Small", 2, "Medium", 3, "Large", 5), "TROUSER CO.", "TROUSER BRAND",
                "TROUSER FASHION LTD.", 10, 26.99f, LocalDate.of(2020, 2, 5), GenderTarget.FEMALES));

        store.registerProduct(new Product(ClothingTypes.SWEATER, "5 632514 785698", "SWEATHER SUPPLY", 5, 26.99f,
                LocalDate.of(2020, 1, 23), GenderTarget.NEUTRAL));
    }

    public void mainMenu() {

        int choice;

        do {
            System.out.println("Clothing store menu: ");
            System.out.println("1. Register a product");
            System.out.println("2. Show overview of products");
            System.out.println("3. Search products");
            System.out.println("4. Remove a product");
            System.out.println("5. Exit");

            choice = ConsoleTool.askUserInputInteger("Your choice: ", 1, 5);
            switch (choice) {
                case 1:
                    registerProduct();
                    break;
                case 2:
                    overviewProducts();
                    break;
                case 3:
                    searchProducts();
                    break;
                case 4:
                    removeProduct();
                    break;
                case 5:
                    break;
            }
        } while (choice != 5);
    }

    static void registerProduct() {
        while (true) {
            ClothingTypes clothingType;
            String barcode;
            Map<String, Integer> fabrics = new HashMap<>();
            Map<String, Integer> sizes;
            String supplier;
            String brand;
            String designer;
            int amount = 0;
            float price;
            LocalDate lastPurchaseDate;
            GenderTarget genderTarget;

            // Ask barcode
            do {
                barcode = ConsoleTool.askUserInputString("What is the barcode: (x xxxxxx xxxxxx | leave empty to exit)");
                if(!StringTool.isBarcode(barcode)) System.err.println("Give a valid barcode\n");
                if(store.getProductByBarcode(barcode) != null) System.err.println("This product is already in the store!");
                if(barcode.length() == 0) return;
            } while(!StringTool.isBarcode(barcode) || store.getProductByBarcode(barcode) != null);

            // Ask clothing type
            do {
                clothingType = ConsoleTool.askUserInputClothingType("What is the clothing type: ");
            } while(clothingType == null);

            // Ask fabrics
            List<String> fabricsList = List.of("Cotton", "Jeans", "Leather", "Linen", "Lycra", "Polyester", "Satin", "Silk", "Suede");
            System.out.print("Setting percentages of ");
            fabricsList.forEach(f -> {
                if(f.equals("Suede")) System.out.print(f + "\n");
                else System.out.print(f + " ");
            });

            int sumPercentage = 0;
            for(String fabric : fabricsList) {
                int percentage;
                do {
                    percentage = ConsoleTool.askUserInputInteger("What is the percentage of " + fabric + ":");
                    sumPercentage += percentage;
                    if(percentage > 100) System.err.println("Percentages cannot be greater than 100 \n");

                    // Making sure total percentage does not exceed 100
                    if (sumPercentage > 100) {
                        percentage = percentage - (sumPercentage - 100);
                        System.err.println("Total percentage exceeded 100, percentage of " + fabric +" is set to " + percentage + "\n");
                    }
                } while (percentage == -1 || percentage > 100);


                fabrics.put(fabric, percentage);
                if(sumPercentage >= 100) break;
            }

            if(sumPercentage < 100) fabrics.put("Unknown", (100 - sumPercentage));

            // Ask sizes
            int small = 0;
            while(small <= 0) {
                small = ConsoleTool.askUserInputInteger("How many pieces of size small are there: ");
                if(small <= 0) System.err.println("Some stock is required!\n");
            }

            int medium = 0;
            while(medium <= 0) {
                medium = ConsoleTool.askUserInputInteger("How many pieces of size medium are there: ");
                if(medium <= 0) System.err.println("Some stock is required!\n");
            }

            int large = 0;
            while(large <= 0) {
                large = ConsoleTool.askUserInputInteger("How many pieces of size large are there: ");
                if(large <= 0) System.err.println("Some stock is required!\n");
            }

            sizes = Map.of("Small", small, "Medium", medium, "large", large);

            // Ask supplier
            supplier = ConsoleTool.askUserInputString("Who is the supplier: ").toUpperCase();

            // Ask brand
            brand = ConsoleTool.askUserInputString("What is the brand: ").toUpperCase();

            // Ask designer
            designer = ConsoleTool.askUserInputString("Who is the designer: ").toUpperCase();

            // Calculating amount
            for(Integer item : sizes.values()) amount += item;

            // Ask price
            do {
                price = ConsoleTool.askUserInputFloat("What is the price: ");
                if(price <= 0) System.err.println("The price cannot be negative or 0\n");
            } while(price <= 0f);

            // Ask last purchase date
            do {
                lastPurchaseDate = ConsoleTool.askUserInputDate("What was the last purchase date: (YYYY-MM-DD)");
            } while (lastPurchaseDate == null);

            // Ask gender target
            do {
                genderTarget = ConsoleTool.askUserInputGenderTarget("What is the gender target: ");
            } while (genderTarget == null);

            // Making a new object
            Product product = new Product(clothingType, barcode, fabrics, sizes, supplier, brand, designer, amount, price, lastPurchaseDate, genderTarget);
            store.registerProduct(product);
            System.out.println("The product is successfully added");
        }
    }

    public void overviewProducts() {
        store.getProducts().stream().sorted(Comparator.comparingInt(Product::getAmount)).forEach(System.out::println);
        ConsoleTool.askPressEnterToContinue();
    }

    public void searchProducts() {
        if(store.getProducts().size() == 0) {
            System.err.println("There are no products in the store!\n");
            return;
        }

        int choice = 0;
        do {
            // Print menu
            System.out.println("Search products menu: ");
            System.out.println("1. Search by barcode");
            System.out.println("2. Search by clothing type");
            System.out.println("3. Search by brand");
            System.out.println("4. Search by supplier");
            System.out.println("5. Search by gender target");
            System.out.println("6. Search by purchase date between two dates");
            System.out.println("7. Search by amount of products available");
            System.out.println("8. Exit");

            // Getting the choice
            choice = ConsoleTool.askUserInputInteger("Your choice: ", 1, 8);
            switch(choice) {
                case 1:
                    filterProductsByBarcode();
                    break;
                case 2:
                    filterProductsByClothingType();
                    break;
                case 3:
                    filterProductsByBrand();
                    break;
                case 4:
                    filterProductsBySupplier();
                    break;
                case 5:
                    filterProductsByGenderTarget();
                    break;
                case 6:
                    filterProductsByPurchaseDate();
                    break;
                case 7:
                    filterProductsBelowAnAmount();
                    break;
                case 8:
                    break;
            }
        } while (choice != 8);
    }

    public void removeProduct() {
        if(store.getProducts().size() == 0) {
            System.err.println("No products to be removed");
            return;
        }

        while(true) {

            Product product;
            String barcode;
            do {
                barcode = ConsoleTool.askUserInputString("Product barcode: (press enter to return)");
                if(barcode.length() == 0) return;
//                if(!ClothingStore.StringTool.isBarcode(barcode)) System.err.println("Give a valid barcode\n");
                product = store.getProductByBarcode(barcode);
                if(product == null) System.err.println("Product not found!");
            } while(product == null);

            // Asking for confirmation
            String confirmation;
            do {
                confirmation = ConsoleTool.askUserInputString("(y/n) to remove " + product);
                confirmation = confirmation.toLowerCase();
            } while (!(confirmation.equals("y") || (confirmation.equals("n"))));

            if(confirmation.equals("y")) {
                if(store.removeProduct(store.getProductByBarcode(barcode))){
                    System.out.println("Product successfully removed");
                } else System.err.println("Error while removing product\n");
            } else {
                System.out.println("Leaving product in the store");
            }
        }
    }

    public void filterProductsByBarcode() {
        String barcode = StringTool.cleanBarcode(ConsoleTool.askUserInputString("Barcode: ", 8));
        List<Product> list = store.getProducts().stream().filter(p -> p.getBarcode().contains(barcode)).sorted(Comparator.comparingInt(Product::getAmount))
                .collect(Collectors.toCollection(ArrayList::new));
        if(list.size() == 0) System.err.println("No products were found\n");
        else ConsoleTool.printProducts(list, "Products which have " + barcode + " in their barcode");
    }

    public void filterProductsByClothingType() {
        ClothingTypes ct = ConsoleTool.askUserInputClothingType("Clothing type: ");
        List<Product> list = store.getProducts().stream().filter(p -> p.getClothingType().equals(ct)).sorted(Comparator.comparingInt(Product::getAmount))
                .collect(Collectors.toCollection(ArrayList::new));
        if(list.size() == 0) System.err.println("No products were found\n");
        else ConsoleTool.printProducts(list, "Clothes of type " + ct);
    }

    public void filterProductsByBrand() {
        String brand = ConsoleTool.askUserInputString("Brand: ", 1).toUpperCase();
        List<Product> list = store.getProducts().stream().filter(p -> p.getBrand().contains(brand)).sorted(Comparator.comparing(Product::getBrand))
                .collect(Collectors.toCollection(ArrayList::new));
        if(list.size() == 0) System.err.println("No products were found\n");
        else ConsoleTool.printProducts(list, "Products of brand " + brand);
    }

    public void filterProductsBySupplier() {
        String supplier = ConsoleTool.askUserInputString("Supplier: ", 1).toUpperCase();
        List<Product> list = store.getProducts().stream().filter(p -> p.getSupplier().contains(supplier)).sorted(Comparator.comparing(Product::getSupplier))
                .collect(Collectors.toCollection(ArrayList::new));
        if (list.size() == 0) System.err.println("No products were found\n");
        else ConsoleTool.printProducts(list,"Products of supplier " + supplier);
    }

    public void filterProductsByGenderTarget() {
        GenderTarget gt = ConsoleTool.askUserInputGenderTarget("Gender target: ");
        List<Product> list = store.getProducts().stream().filter(p -> p.getGenderTarget().equals(gt)).sorted(Comparator.comparing(Product::getClothingType))
                .collect(Collectors.toCollection(ArrayList::new));
        if(list.size() == 0) System.err.println("No products were found\n");
        else ConsoleTool.printProducts(list, "Products for " + gt);
    }

    public void filterProductsByPurchaseDate() {
        LocalDate startDate = ConsoleTool.askUserInputDateBefore("Start date: ", LocalDate.now());
        LocalDate endDate = ConsoleTool.askUserInputDateBetween("End date: ", startDate, LocalDate.now().plusDays(1));

        List<Product> list = store.getProducts().stream().filter(p ->
            p.getLastPurchaseDate().isAfter(startDate) || p.getLastPurchaseDate().isEqual(startDate) &&
            p.getLastPurchaseDate().isBefore(endDate) || p.getLastPurchaseDate().isEqual(endDate)
        ).sorted(Comparator.comparing(Product::getLastPurchaseDate)).collect(Collectors.toCollection(ArrayList::new));
        if(list.size() == 0) System.err.println("No products were found\n");
        else ConsoleTool.printProducts(list, "Products with last purchased date between " + startDate + " and " + endDate);
    }

    public void filterProductsBelowAnAmount() {
        int amount = ConsoleTool.askUserInputInteger("Amount in stock: ", 1);
        List<Product> list = store.getProducts().stream().filter(p -> p.getAmount() < amount).sorted(Comparator.comparingInt(Product::getAmount))
                .collect(Collectors.toCollection(ArrayList::new));
        if (list.size() == 0) System.err.println("No products were found\n");
        else ConsoleTool.printProducts(list, "Products with less than " + amount + " in stock");
    }
}