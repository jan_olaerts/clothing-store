package be.vdab.ClothingStore;

public enum ClothingTypes {
    TROUSERS,
    SHIRT,
    POLO,
    SWEATER,
    DRESS
}