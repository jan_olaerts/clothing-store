package be.vdab.ClothingStore;

import java.time.LocalDate;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Product {

    private ClothingTypes clothingType;
    private String barcode;
    private Map<String, Integer> fabrics;
    private Map<String, Integer> sizes;
    private String supplier;
    private String brand;
    private String designer;
    private int amount;
    private float price;
    private LocalDate lastPurchaseDate;
    private GenderTarget genderTarget;

    public Product(ClothingTypes clothingType, String barcode, String supplier, int amount, float price, LocalDate lastPurchaseDate,
                   GenderTarget genderTarget) {
        this(clothingType, barcode, new HashMap<>(), new HashMap<>(), supplier, "UNKNOWN", "UNKNOWN", amount, price, lastPurchaseDate,
                genderTarget);
    }

    public Product(ClothingTypes clothingType, String barcode, Map<String, Integer> fabrics, Map<String, Integer> sizes, String supplier,
                   String brand, String designer, int amount, float price, LocalDate lastPurchaseDate, GenderTarget genderTarget) {
        this.clothingType = clothingType;
        this.barcode = barcode;
        this.fabrics = fabrics;
        this.sizes = sizes;
        this.supplier = supplier;
        this.brand = brand;
        this.designer = designer;
        this.amount = amount;
        this.price = price;
        this.lastPurchaseDate = lastPurchaseDate;
        this.genderTarget = genderTarget;
    }

    public ClothingTypes getClothingType() {
        return clothingType;
    }


    public String getBarcode() {
        return barcode;
    }


    public Map<String, Integer> getFabrics() {
        return fabrics;
    }


    public Map<String, Integer> getSizes() {
        return sizes;
    }


    public String getSupplier() {
        return supplier;
    }


    public String getBrand() {
        return brand;
    }

    public int getAmount() {
        return amount;
    }

    public LocalDate getLastPurchaseDate() {
        return lastPurchaseDate;
    }

    public GenderTarget getGenderTarget() {
        return genderTarget;
    }

    @Override
    public String toString() {
        Formatter formatter = new Formatter();
        return formatter.format("Product: Type = %-9s | Barcode = %10s | Brand = %15s | Supplier = %15s | Gender = %10s" +
                        " | Last purchase date = %10s | Amount = %7s",
                clothingType, StringTool.cleanBarcode(barcode), brand, supplier, genderTarget, lastPurchaseDate, amount).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return amount == product.amount &&
                Float.compare(product.price, price) == 0 &&
                clothingType == product.clothingType &&
                Objects.equals(barcode, product.barcode) &&
                Objects.equals(fabrics, product.fabrics) &&
                Objects.equals(sizes, product.sizes) &&
                Objects.equals(supplier, product.supplier) &&
                Objects.equals(brand, product.brand) &&
                Objects.equals(designer, product.designer) &&
                Objects.equals(lastPurchaseDate, product.lastPurchaseDate) &&
                genderTarget == product.genderTarget;
    }

    @Override
    public int hashCode() {
        return Objects.hash(clothingType, barcode, fabrics, sizes, supplier, brand, designer, amount, price, lastPurchaseDate, genderTarget);
    }
}